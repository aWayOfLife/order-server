const express = require('express')
const router  = express.Router()
const { v4: uuidv4 } = require('uuid');
const sendOrderMessage = require('../core/send-order-message');
const pool = require('../core/postgres');

router.post('/process', async (req, res) =>{
    try{
        const { orderNumber, orderStatus } = req?.body;
        const orderId = uuidv4();
        pool.query('INSERT INTO orders_processed (order_number, order_status, order_id) VALUES ($1, $2, $3)', [orderNumber, orderStatus, orderId], async (error, result) => {
            if (error) {
                return res.status(400).send(error)
            }
            pool.query('SELECT * FROM orders_processed WHERE order_id = $1',[orderId], async(error, result) => {
                if (error) {
                    return res.status(400).send(error)
                }
                if (result.rows[0]) {
                    await sendOrderMessage({
                        orderNumber: result.rows[0].order_number,
                        orderStatus: result.rows[0].order_status,
                        orderId: result.rows[0].order_id
                    })
                }
                return res.status(201).send('order created');
            })
        })
    }catch(err){
        return res.status(400).send(err)
    }
})

router.post('/receive', async (req, res) =>{
    try{
        pool.query('SELECT status FROM health WHERE id = 1',(error, result) => {
            if (error) {
                return res.status(400).send(error)
            }
            if (result.rows[0].status) {
                const { orderNumber, orderStatus, orderId } = req?.body;
                pool.query('INSERT INTO orders_received (order_number, order_status, order_id) VALUES ($1, $2, $3)', [orderNumber, orderStatus, orderId], (error, result) => {
                    if (error) {
                        return res.status(400).send(error)
                    }
                    return res.status(201).json({result: req?.body});
                })
            } else {
                return res.status(500).send('server is down');
            }
        })
    }catch(err){
        return res.status(400).send(err)
    }
})

module.exports = router

