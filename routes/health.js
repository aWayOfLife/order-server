const express = require('express');
const router = express.Router();

const pool = require('../core/postgres');

router.get('/', async (req, res) => {
  try {
    pool.query('SELECT status FROM health WHERE id = 1',(error, result) => {
        if (error) {
            return res.status(400).send(error)
        }
        if (result.rows[0].status) {
            return res.status(200).json({server: 'up'});
        } else {
            return res.status(500).send({server: 'down'});
        }
    })
  } catch (err) {
    return res.status(400).send(err);
  }
});

router.post('/', async (req, res) => {
    const { status } = req?.body;
    try {
        pool.query('UPDATE health SET status = $1 WHERE id = 1',[status], (error, result) => {
            if (error) {
                return res.status(400).send(error)
            }
            return res.status(200).send('server status updated')
        })
  } catch (err) {
    return res.status(400).send(err);
  }
});

module.exports = router;
