const express = require('express');
const cors = require('cors');

const app = express();

const healthRoute = require('./routes/health');
const orderRoute = require('./routes/order');

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  }),
);
app.use(cors());

app.use('/api/health', healthRoute);
app.use('/api/order', orderRoute);

app.listen(process.env.PORT || 5000, () => console.log('Server is up'));
