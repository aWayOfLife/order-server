const { ServiceBusClient } = require("@azure/service-bus");
const serviceBusClient = new ServiceBusClient(process.env.SB_RETRY);
const queueName = 'orders-primary';
const sender = serviceBusClient.createSender(queueName);
const sendOrderMessage = async(order) => {
    try {
        await sender.sendMessages({
            body: order,
            contentType: 'application/json'
        });
        console.log(`Success: Order ${order.orderNumber} pushed to orders-primary`)
    }catch(error) {
        console.log(`Error: Failed to push order ${order.orderNumber} to orders-primary`)
    }
}

module.exports = sendOrderMessage;